from os import environ, path

DATABASE_USER = environ.get("DATABASE_USER", "postgre")
DATABASE_PASSWORD = environ.get("DATABASE_PASSWORD", "postgre")
DATABASE_HOST = environ.get("DATABASE_HOST", "localhost")
DATABASE_NAME = environ.get("DATABASE_NAME", "pixel_map_admin")
