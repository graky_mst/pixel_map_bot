import typing
import datetime
import sqlalchemy
from sqlalchemy import create_engine, Column, text, ForeignKey
from sqlalchemy.orm import (
    sessionmaker,
    declarative_base,
    Session as BaseSession,
    relationship,
)
from sqlalchemy.orm.exc import FlushError
from sqlalchemy.event import listen
from sqlalchemy_fields.storages import FileSystemStorage
from sqlalchemy_fields.types import FileType, ImageType
from sqlalchemy.dialects.postgresql import UUID
from pixel_map_admin.bot import message_send
from database.settings import (
    DATABASE_USER,
    DATABASE_PASSWORD,
    DATABASE_HOST,
    DATABASE_NAME,
)

SQLALCHEMY_DATABASE_URL = f"postgresql://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:5432/{DATABASE_NAME}"
engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    pool_pre_ping=True,
    pool_size=20,
    max_overflow=0,
    echo=False,
)
Base: typing.Any = declarative_base()


class User(Base):
    __tablename__ = "pixel_map_user"
    uuid = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        server_default=text("uuid_generate_v4()"),
    )
    email = Column(sqlalchemy.String(254), nullable=True, unique=True)
    phone = Column(sqlalchemy.String(128), nullable=True, unique=True)
    first_name = Column(sqlalchemy.String(30), default="")
    last_name = Column(sqlalchemy.String(30), default="")
    telegram_id = Column(sqlalchemy.Integer, unique=True)
    enrolled_in_intensive = Column(sqlalchemy.Boolean, default=False)
    user_answer = relationship(
        "UserAnswerResult",
        cascade="all,delete",
        back_populates="user",
    )

    def __repr__(self):
        return f"{self.first_name} {self.last_name} {self.uuid}"


class Quiz(Base):
    __tablename__ = "quiz"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    question_text = Column(sqlalchemy.String(254), nullable=True, unique=True)
    position = Column(sqlalchemy.Integer, default=1)
    answer = relationship(
        "QuizAnswer",
        cascade="all,delete",
        back_populates="quiz",
        order_by="QuizAnswer.position",
    )
    user_answer = relationship(
        "UserAnswerResult",
        cascade="all,delete",
        back_populates="quiz",
    )

    def __repr__(self):
        return self.question_text


class QuizAnswer(Base):
    __tablename__ = "quiz_answer"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    answer_text = Column(sqlalchemy.String(40), nullable=False)
    quiz_id = Column(
        sqlalchemy.Integer,
        ForeignKey(Quiz.id, ondelete="CASCADE"),
    )
    quiz = relationship("Quiz", foreign_keys="QuizAnswer.quiz_id")
    position = Column(sqlalchemy.Integer, default=1)
    is_correct = Column(sqlalchemy.Boolean, default=False)
    user_answer = relationship(
        "UserAnswerResult",
        cascade="all,delete",
        back_populates="answer",
    )

    def __repr__(self):
        return self.answer_text


class UserAnswerResult(Base):
    __tablename__ = "user_answer_result"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    user_id = Column(
        UUID,
        ForeignKey(User.uuid, ondelete="CASCADE"),
    )
    user = relationship("User", foreign_keys="UserAnswerResult.user_id")
    quiz_id = Column(
        sqlalchemy.Integer,
        ForeignKey(Quiz.id, ondelete="CASCADE"),
    )
    quiz = relationship("Quiz", foreign_keys="UserAnswerResult.quiz_id")
    answer_id = Column(
        sqlalchemy.Integer,
        ForeignKey(QuizAnswer.id, ondelete="CASCADE"),
    )
    answer = relationship("QuizAnswer", foreign_keys="UserAnswerResult.answer_id")

    def __repr__(self):
        with Session() as session:
            user_answer = session.query(UserAnswerResult).get(self.id)
            return f"{user_answer.user.first_name}, {user_answer.quiz_id}, {user_answer.answer.id}"


class MessageToSend(Base):
    __tablename__ = "message_to_send"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    message_text = Column(sqlalchemy.Text, nullable=True)
    datetime_to_send = Column(sqlalchemy.DateTime, nullable=True)
    already_sent = Column(sqlalchemy.Boolean, default=False)
    images = relationship(
        "ImagesToSend",
        cascade="all,delete",
        back_populates="message",
    )
    files = relationship(
        "FileToSend",
        cascade="all,delete",
        back_populates="message",
    )
    to_users = relationship(
        "UsersToSend",
        cascade="all,delete",
        back_populates="message",
    )

    def __repr__(self):
        return f"{self.id}, {self.message_text[:30]}"

    @classmethod
    async def send_to_user(cls):
        with Session() as session:
            messages_to_send = (
                session.query(cls)
                .filter(
                    cls.datetime_to_send < datetime.datetime.now(),
                    cls.already_sent == False,
                )
                .all()
            )
            user_telegram_ids = []
            images_to_send = []
            files_to_send = []
            for message_to_send in messages_to_send:
                if message_to_send.to_users:
                    for user in message_to_send.to_users:
                        user_telegram_ids.append(user.user.telegram_id)
                else:
                    users = session.query(User).all()
                    for user in users:
                        user_telegram_ids.append(user.telegram_id)
                if message_to_send.images:
                    for image in message_to_send.images:
                        images_to_send.append(image.image.path)
                if message_to_send.files:
                    for file in message_to_send.files:
                        files_to_send.append(file.file.path)
                for user in user_telegram_ids:
                    await message_send(
                        user_id=user,
                        files=files_to_send,
                        images=images_to_send,
                        message_text=message_to_send.message_text,
                    )
                message_to_send.already_sent = True
                session.add(message_to_send)
                session.commit()


class ImagesToSend(Base):
    __tablename__ = "image_to_send"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    image = Column(ImageType(storage=FileSystemStorage(path="media/images/")))
    message_id = Column(
        sqlalchemy.Integer,
        ForeignKey(MessageToSend.id, ondelete="CASCADE"),
    )
    message = relationship("MessageToSend", foreign_keys="ImagesToSend.message_id")


class FileToSend(Base):
    __tablename__ = "file_to_send"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    file = Column(FileType(storage=FileSystemStorage(path="media/files/")))
    message_id = Column(
        sqlalchemy.Integer,
        ForeignKey(MessageToSend.id, ondelete="CASCADE"),
    )
    message = relationship("MessageToSend", foreign_keys="FileToSend.message_id")


class UsersToSend(Base):
    __tablename__ = "users_to_send"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    user_id = Column(
        UUID,
        ForeignKey(User.uuid, ondelete="CASCADE"),
    )
    user = relationship("User", foreign_keys="UsersToSend.user_id")

    message_id = Column(
        sqlalchemy.Integer,
        ForeignKey(MessageToSend.id, ondelete="CASCADE"),
    )
    message = relationship("MessageToSend", foreign_keys="UsersToSend.message_id")


class CommandText(Base):
    __tablename__ = "start_text"
    uuid = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        server_default=text("uuid_generate_v4()"),
    )
    start_text = Column(sqlalchemy.Text, nullable=True, unique=True)
    intensive_text = Column(sqlalchemy.Text, nullable=True, unique=True)
    course_text = Column(sqlalchemy.Text, nullable=True, unique=True)
    community_text = Column(sqlalchemy.Text, nullable=True, unique=True)

    @classmethod
    def get_instance(cls, session):
        instance = session.query(cls).first()
        if not instance:
            instance = cls()
            session.add(instance)
            session.commit()
        return instance


def enforce_singleton_model(session, flush_context, instances):
    for instance in session.new:
        if isinstance(instance, CommandText) and session.query(CommandText).count() > 0:
            raise FlushError(
                "Может существовать только один экземпляр текста для команд"
            )


listen(BaseSession, "before_flush", enforce_singleton_model)


Session = sessionmaker(bind=engine)
