import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from bot_pixel import PixelMapTelegramClient
from settings import (
    TELEGRAM_TOKEN,
    TELEGRAM_APP_API_ID,
    TELEGRAM_APP_API_HASH,
    TELEGRAM_SESSION,
)


bot = PixelMapTelegramClient(
    session=TELEGRAM_SESSION, api_id=TELEGRAM_APP_API_ID, api_hash=TELEGRAM_APP_API_HASH
).start(bot_token=TELEGRAM_TOKEN)

if __name__ == "__main__":
    bot.start()
    print("Bot is running!")
    bot.run_until_disconnected()
