from os import environ, path

TELEGRAM_TOKEN = environ.get("TELEGRAM_TOKEN", "")
TELEGRAM_APP_API_ID = environ.get("TELEGRAM_APP_ID", "")
TELEGRAM_APP_API_HASH = environ.get("TELEGRAM_APP_API_HASH", "")
TELEGRAM_SESSION = environ.get("TELEGRAM_SESSION", "pixel_map")
