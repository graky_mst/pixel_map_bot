import random
import logging
import re
from telethon import TelegramClient, events, functions, types
from buttons import Buttons
from database.models import (
    SQLALCHEMY_DATABASE_URL,
    engine,
    User,
    CommandText,
    Session,
    Quiz,
    QuizAnswer,
    UserAnswerResult,
)
from enum import Enum
from telethon.tl.types import (
    MessageMediaPoll,
    Poll,
    PollAnswer,
    PollAnswerVoters,
    PollResults,
)


class State(Enum):
    WAIT_EMAIL = "WAIT_EMAIL"


class PixelMapTelegramClient(TelegramClient, Buttons):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logging.getLogger().setLevel(logging.INFO)
        self.add_event_handler(self.callback_handler, events.CallbackQuery)
        self.add_event_handler(self.message_handler, events.NewMessage)
        self.conversation_state = {}
        logging.info("INIT BOT")

    async def send_message_to_chat(self, event, message_text, buttons=None):
        chat_id = await event.get_chat()
        await self.send_message(chat_id, message=message_text, buttons=buttons)

    async def send_quiz(self, event, quiz, session):
        with session:
            quez_answers = quiz.answer
            poll_answers = []
            chat_id = await event.get_chat()
            for answer in quez_answers:
                if answer.is_correct:
                    correct_position = answer.position
                poll_answer = PollAnswer(
                    text=answer.answer_text, option=bytes([answer.position])
                )
                poll_answers.append(poll_answer)
            poll = Poll(
                id=random.randint(0, 1000000),
                question=quiz.question_text,
                answers=poll_answers,
                quiz=True,
                public_voters=True,
            )
            message = await self.send_message(
                chat_id,
                file=MessageMediaPoll(
                    poll=poll,
                    results=PollResults(
                        results=[
                            PollAnswerVoters(
                                option=bytes([correct_position]), voters=1, correct=True
                            )
                        ],
                    ),
                ),
            )
            return message

    async def command_handler(self, event):
        command, *args = event.message.message.split()
        match command:
            case "/start":
                with Session() as session:
                    start_text = getattr(
                        session.query(CommandText).first(), "start_text", ""
                    )
                await self.send_message_to_chat(
                    event=event, message_text=start_text, buttons=self.start_buttons()
                )
            case _:
                print(command)

    async def callback_handler(self, event):
        chat_id = await event.get_chat()
        telegram_id = event.original_update.user_id
        callback_list = event.data.decode("utf-8").split()
        if callback_list[0] == "qu":
            ques_id = callback_list[1]
            message = await self.get_messages(chat_id, ids=int(callback_list[2]))
            poll = message.poll
            poll_answer = poll.poll.answers
            results = poll.results.results
            selected_answers_options = list(
                map(
                    lambda answer: answer.option,
                    filter(lambda answer: answer.voters > 0, results),
                )
            )
            selected_answers = list(
                filter(
                    lambda answer: answer.option in selected_answers_options,
                    poll_answer,
                )
            )[0]
            with Session() as session:
                user = (
                    session.query(User).filter(User.telegram_id == telegram_id).first()
                )
                answer = (
                    session.query(QuizAnswer)
                    .filter(
                        QuizAnswer.quiz_id == int(ques_id),
                        QuizAnswer.answer_text == selected_answers.text,
                    )
                    .first()
                )
                user_answer = UserAnswerResult(
                    quiz_id=int(ques_id), answer_id=answer.id, user_id=user.uuid
                )
                session.add(user_answer)
                session.commit()
                answered_questions = list(
                    map(
                        lambda user_answer: user_answer[0],
                        session.query(UserAnswerResult.quiz_id)
                        .filter(UserAnswerResult.user_id == user.uuid)
                        .all(),
                    )
                )
                not_answered_question = (
                    session.query(Quiz)
                    .filter(Quiz.id.not_in(answered_questions))
                    .first()
                )
                if not_answered_question is None:
                    await self.send_message_to_chat(
                        event=event,
                        message_text="Вы уже прошли тест!",
                        buttons=self.back_button(),
                    )
                else:
                    message = await self.send_quiz(
                        event=event, quiz=not_answered_question, session=session
                    )
                    await self.send_message_to_chat(
                        event=event,
                        message_text="-",
                        buttons=self.next_question(
                            message_id=message.id, ques_id=not_answered_question.id
                        ),
                    )

    async def message_handler(self, event):
        message_text = event.message.message
        user = await event.get_sender()
        if message_text.startswith("/"):
            await self.command_handler(event=event)
        match message_text:
            case "Записаться на интенсив":
                with Session() as session:
                    database_user = (
                        session.query(User).filter(User.telegram_id == user.id).first()
                    )
                    if database_user is not None and getattr(
                        database_user, "enrolled_in_intensive", False
                    ):
                        response = "Вы уже записаны на интенсив!"
                    else:
                        response = "Отправьте свою электронную посту!"
                await self.send_message_to_chat(
                    event=event, message_text=response, buttons=self.back_button()
                )
                self.conversation_state[user.id] = State.WAIT_EMAIL
            case "Научиться веб-дизайну":
                with Session() as session:
                    database_user = (
                        session.query(User).filter(User.telegram_id == user.id).first()
                    )
                    if database_user is None:
                        new_user = User(
                            telegram_id=user.id,
                            first_name=user.first_name,
                            last_name=user.last_name,
                            phone=user.phone,
                        )
                        session.add(new_user)
                        session.commit()
                    response = session.query(CommandText).first().course_text
                await self.send_message_to_chat(
                    event=event, message_text=response, buttons=self.back_button()
                )
            case "Попасть в комьюнити дизайнеров":
                with Session() as session:
                    database_user = (
                        session.query(User).filter(User.telegram_id == user.id).first()
                    )
                    if database_user is None:
                        new_user = User(
                            telegram_id=user.id,
                            first_name=user.first_name,
                            last_name=user.last_name,
                            phone=user.phone,
                        )
                        session.add(new_user)
                        session.commit()
                    response = session.query(CommandText).first().community_text
                await self.send_message_to_chat(
                    event=event, message_text=response, buttons=self.back_button()
                )
            case "Узнать свой реальный уровень":
                with Session() as session:
                    database_user = (
                        session.query(User).filter(User.telegram_id == user.id).first()
                    )
                    if database_user is None:
                        new_user = User(
                            telegram_id=user.id,
                            first_name=user.first_name,
                            last_name=user.last_name,
                            phone=user.phone,
                        )

                        session.add(new_user)
                        session.commit()
                        session.refresh(new_user)
                        user_uuid = new_user.uuid
                    else:
                        user_uuid = database_user.uuid

                    answered_questions = list(
                        map(
                            lambda user_answer: user_answer[0],
                            session.query(UserAnswerResult.quiz_id)
                            .filter(UserAnswerResult.user_id == user_uuid)
                            .all(),
                        )
                    )
                    not_answered_question = (
                        session.query(Quiz)
                        .filter(Quiz.id.not_in(answered_questions))
                        .first()
                    )
                    if not_answered_question is None:
                        await self.send_message_to_chat(
                            event=event,
                            message_text="Вы уже прошли тест!",
                            buttons=self.back_button(),
                        )
                    else:
                        message = await self.send_quiz(
                            event=event, quiz=not_answered_question, session=session
                        )
                        await self.send_message_to_chat(
                            event=event,
                            message_text="-",
                            buttons=self.next_question(
                                message_id=message.id, ques_id=not_answered_question.id
                            ),
                        )

            case "Вернуться в главное меню":
                self.conversation_state[user.id] = None
                event.message.message = "/start"
                await self.command_handler(event=event)
            case _:
                if self.conversation_state.get(user.id, None) == State.WAIT_EMAIL:
                    user_email = message_text
                    email_regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
                    if bool(re.match(email_regex, user_email)):
                        with Session() as session:
                            new_user = (
                                session.query(User)
                                .filter(User.telegram_id == user.id)
                                .first()
                            )
                            if new_user:
                                if (
                                    session.query(User)
                                    .filter(User.email == user_email)
                                    .first()
                                    is None
                                ):
                                    new_user.enrolled_in_intensive = True
                                    new_user.email = user_email
                                else:
                                    await self.send_message_to_chat(
                                        event=event,
                                        message_text="Электронная почта уже занята, попробуйте другую",
                                        buttons=self.back_button(),
                                    )
                                    return False
                            else:
                                new_user = User(
                                    email=user_email,
                                    telegram_id=user.id,
                                    first_name=user.first_name,
                                    last_name=user.last_name,
                                    phone=user.phone,
                                    enrolled_in_intensive=True,
                                )
                            response = session.query(CommandText).first().intensive_text
                            session.add(new_user)
                            session.commit()
                        self.conversation_state[user.id] = None

                        await self.send_message_to_chat(
                            event=event,
                            message_text=response,
                            buttons=self.back_button(),
                        )
                    else:
                        await self.send_message_to_chat(
                            event=event,
                            message_text="Электронная почта написана некорректно, попробуйте ещё раз",
                            buttons=self.back_button(),
                        )
