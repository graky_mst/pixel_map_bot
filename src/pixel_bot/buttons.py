from telethon import Button


class Buttons:
    def start_buttons(self):
        buttons_ = [
            [Button.text("Записаться на интенсив", resize=True)],
            [Button.text("Научиться веб-дизайну", resize=True)],
            # [Button.text("Узнать свой реальный уровень", resize=True)],
            [Button.text("Попасть в комьюнити дизайнеров")],
        ]
        return buttons_

    def back_button(self):
        buttons_ = [
            Button.text("Вернуться в главное меню", resize=True),
        ]
        return buttons_

    def next_question(self, ques_id, message_id):
        buttons = [Button.inline("Следующий вопрос", f"qu {ques_id} {message_id}")]
        return buttons
