import datetime
import typing
import json
from collections.abc import Mapping
from pixel_map_admin import settings
from sqladmin.authentication import AuthenticationBackend
from starlette.requests import Request
from starlette.responses import RedirectResponse
from jwt import InvalidSignatureError, api_jws, decode, ExpiredSignatureError
from calendar import timegm


def encode(
    payload: typing.Dict[str, typing.Any],
    key: str,
    algorithm: typing.Optional[str] = "HS256",
    headers: typing.Optional[typing.Dict] = None,
    json_encoder: typing.Optional[typing.Type[json.JSONEncoder]] = None,
) -> str:
    if not isinstance(payload, Mapping):
        raise TypeError("Токен не является JSON форматом")
    payload = payload.copy()
    for time_claim in ["exp", "origIat", "nbf"]:
        if isinstance(payload.get(time_claim), datetime.datetime):
            payload[time_claim] = timegm(payload[time_claim].utctimetuple())

    json_payload = json.dumps(payload, separators=(",", ":"), cls=json_encoder).encode(
        "utf-8"
    )

    return api_jws.encode(json_payload, key, algorithm, headers, json_encoder)


class AdminAuth(AuthenticationBackend):
    async def login(self, request: Request) -> bool:
        form = await request.form()
        username, password = form["username"], form["password"]
        if username == settings.ADMIN_USERNAME and password == settings.ADMIN_PASSWORD:
            iat = datetime.datetime.utcnow()
            expire = datetime.datetime.utcnow() + datetime.timedelta(days=1)
            payload = {
                "username": username,
                "exp": expire,
                "origIat": iat,
                "type": "token",
            }
            token = encode(payload, settings.SECRET_KEY, algorithm="HS256")
            request.session.update({"token": token})
            return True

    async def logout(self, request: Request) -> bool:
        request.session.clear()
        return True

    async def authenticate(self, request: Request) -> RedirectResponse:
        token = request.session.get("token", None)
        if not token:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)
        try:
            payload = decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        except InvalidSignatureError:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)
        except ExpiredSignatureError:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)
        username = payload.get("username")
        if username != settings.ADMIN_USERNAME:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)
