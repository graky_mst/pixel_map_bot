from os import environ, path

ADMIN_USERNAME = environ.get("ADMIN_USERNAME", "pixel_map_admin")
ADMIN_PASSWORD = environ.get("ADMIN_PASSWORD", "pixel_map_admin")

SECRET_KEY = environ.get("SECRET_KEY", "qZ-OOxs2wYSHzGk8LmFHjRixmwCbVVjMbnAAaM1v-_0=")
TELEGRAM_TOKEN = environ.get("TELEGRAM_TOKEN", "")
TELEGRAM_APP_API_ID = environ.get("TELEGRAM_APP_ID", "")
TELEGRAM_APP_API_HASH = environ.get("TELEGRAM_APP_API_HASH", "")
TELEGRAM_SESSION = environ.get("TELEGRAM_SESSION", "pixel_map")
BASE_DIR = path.dirname(path.abspath(__file__))
