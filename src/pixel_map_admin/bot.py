from telethon import TelegramClient

from pixel_map_admin import settings


async def bot_init() -> TelegramClient:
    bot = TelegramClient(
        session=settings.TELEGRAM_SESSION,
        api_id=settings.TELEGRAM_APP_API_ID,
        api_hash=settings.TELEGRAM_APP_API_HASH,
    )

    return await bot.start(bot_token=settings.TELEGRAM_TOKEN)


async def message_send(user_id, files, images, message_text):
    message_len = len(message_text)
    async with await bot_init() as bot:
        if files:
            if message_len < 1024:
                if len(files) > 1:
                    await bot.send_file(user_id, file=files[0 : len(files) - 1])
                await bot.send_file(
                    user_id,
                    caption=message_text,
                    file=files[-1],
                )
                return True
            else:
                await bot.send_file(user_id, file=files)
        if images:
            if message_len < 1024:
                if len(images) > 1:
                    await bot.send_file(user_id, file=images[0 : len(files) - 1])
                await bot.send_file(
                    user_id,
                    caption=message_text,
                    file=images[-1],
                )
                return True
            else:
                await bot.send_file(user_id, file=images)
        for i in range(0, message_len, 4096):
            await bot.send_message(user_id, message=message_text[i : i + 4096])
