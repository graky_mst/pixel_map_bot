from fastapi_sqlalchemy import DBSessionMiddleware
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from database.models import SQLALCHEMY_DATABASE_URL, engine, MessageToSend
from fastapi_amis_admin.admin import AdminSite, Settings
from fastapi_scheduler import SchedulerAdmin
from sqladmin import Admin
from pixel_map_admin.auth import AdminAuth
from pixel_map_admin.admin import MODEL_ADMIN_LIST
from pixel_map_admin.settings import SECRET_KEY


def create_app():
    app = FastAPI()
    app.add_middleware(DBSessionMiddleware, db_url=SQLALCHEMY_DATABASE_URL)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
        expose_headers=["*"],
    )

    return app


application = create_app()
authentication_backend = AdminAuth(secret_key=SECRET_KEY)
admin = Admin(application, engine, authentication_backend=authentication_backend)


for admin_model in MODEL_ADMIN_LIST:
    admin.add_view(admin_model)


site = AdminSite(
    settings=Settings(database_url_async="sqlite+aiosqlite:///amisadmin.db")
)
scheduler = SchedulerAdmin.bind(site)


@scheduler.scheduled_job("interval", minutes=5)
async def cron_task_update_message_sender():
    await MessageToSend.send_to_user()


@application.on_event("startup")
async def startup():
    scheduler.start()
