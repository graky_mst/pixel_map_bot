from sqladmin import ModelView
from database.models import (
    User,
    CommandText,
    Quiz,
    QuizAnswer,
    UserAnswerResult,
    MessageToSend,
    ImagesToSend,
    FileToSend,
    UsersToSend,
)


class UserAdmin(ModelView, model=User):
    column_list = [User.first_name, User.telegram_id, User.email]


class ComandAdmin(ModelView, model=CommandText):
    column_list = [CommandText.start_text]


class QuizAdmin(ModelView, model=Quiz):
    column_list = [Quiz.question_text]


class QuizAnswerAdmin(ModelView, model=QuizAnswer):
    column_list = [QuizAnswer.answer_text, QuizAnswer.quiz_id, QuizAnswer.is_correct]


class UserAnswerAdmin(ModelView, model=UserAnswerResult):
    column_list = [
        UserAnswerResult.user_id,
        UserAnswerResult.quiz_id,
        UserAnswerResult.answer_id,
    ]


class MessageToSendAdmin(ModelView, model=MessageToSend):
    column_list = [
        MessageToSend.id,
        MessageToSend.datetime_to_send,
        MessageToSend.already_sent,
    ]


class ImagesAdmin(ModelView, model=ImagesToSend):
    column_list = [ImagesToSend.id, ImagesToSend.message_id]


class FilesAdmin(ModelView, model=FileToSend):
    column_list = [FileToSend.id, FileToSend.message_id]


class UsersToSendAdmin(ModelView, model=UsersToSend):
    column_list = [UsersToSend.id, UsersToSend.message_id, UsersToSend.user_id]


MODEL_ADMIN_LIST = [
    UserAdmin,
    ComandAdmin,
    QuizAdmin,
    QuizAnswerAdmin,
    UserAnswerAdmin,
    MessageToSendAdmin,
    ImagesAdmin,
    FilesAdmin,
    UsersToSendAdmin,
]
