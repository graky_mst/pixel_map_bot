FROM python:3.10-slim-buster

RUN apt-get update \
    && apt-get -y install libpq-dev gcc git \
    && pip install psycopg2

RUN python -m pip install pipenv

WORKDIR /app

ADD Pipfile Pipfile.lock /app/

RUN pipenv install --system --deploy --ignore-pipfile

COPY src/. /app
COPY entrypoint.sh /app

RUN chmod +x /app/entrypoint.sh

ENTRYPOINT ["sh", "./entrypoint.sh"]