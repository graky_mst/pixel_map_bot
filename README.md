# PixelMap телеграм бот

Проект состоит из 3 модулей:
- админ панель на FastAPI 
- телеграм бот на Telethon
- база данных PostgreSQL, SQLAlchemy в качестве ORM 


## Установка проекта:

- `pipenv install` - установка зависимостей
- `pipenv shell` - активация виртуального окружения

### База данных
В .env задать переменные окружения:
- DATABASE_USER = "postgres_acc" - пользователь с доступом к базе данных 
- DATABASE_PASSWORD = "password" - пароль 
- DATABASE_HOST = "localhost" - хост
- DATABASE_NAME = "pixel_map" - имя базы данных

Провести миграцию базы данных: 
- `cd src`
- `alembic upgrade head`

Команда для создания новой миграции - `alembic revision --message="Some text" --autogenerate`

В папке src/database/migrations/versions появится новый .py файл с именем состоящим из хэша и message из команды

Применение конкретной миграции - alembic upgrade ae1027a6acf


### Админ панель
В .env задать переменные окружения:
- ADMIN_USERNAME = "pixel_map_admin" - логин для входа в админ панель
- ADMIN_PASSWORD = "pixel_map_admin" - пароль для входа в админ панель
- SECRET_KEY = "qZ-OOxs2wYSHzGk8LmFHjRixmwCbVVjMbnAAaM1v-_0=" - ключ для JWT авторизации
- TELEGRAM_TOKEN = "dsgssadd-saaw12323fd53113" токен бота, получать в BotFather боте в телеграме 
- TELEGRAM_APP_API_ID = "1224313" - id приложения, получать на https://my.telegram.org/apps 
- TELEGRAM_APP_API_HASH = "dsgssadd-saaw12323fd53113" - хэш апи ключа, получать на https://my.telegram.org/apps 
- TELEGRAM_SESSION = "pixel_map" - файл сессии бота


Запуск сервиса админ панель: 
- `cd src`
- `uvicorn pixel_map_admin.main:application --host 0.0.0.0 --port 8000 --proxy-headers --reload`

Вход в админ панель: 
http://127.0.0.1:8000/admin/login

Модели:
Users - пользователи бота
Command texts - ответы для команд:
- Start text - ответ на команду /start
- Intensive Text - ответ на кнопку "Записаться на интенсив"
- Course Text - ответ на кнопку "Научиться веб-дизайну"
- Community Text - ответ на кнопку "Попасть в комьюнити дизайнеров"

Quizs - вопросы для теста
Quiz Answers - ответы на вопросы
User Answer Results - ответы пользователей на вопросы
Message  To Sends - сообщения для рассылки
Images To Sends - изображения для сообщений в рассылке
Files To Sends - файлы для сообщений в рассылке
Users To Sends - указать пользователей для рассылки, если пользователи не указаны, рассылается всем


### Телеграм бот
В .env задать переменные окружения:
- TELEGRAM_TOKEN = "dsgssadd-saaw12323fd53113" токен бота, получать в BotFather боте в телеграме 
- TELEGRAM_APP_API_ID = "1224313" - id приложения, получать на https://my.telegram.org/apps 
- TELEGRAM_APP_API_HASH = "dsgssadd-saaw12323fd53113" - хэш апи ключа, получать на https://my.telegram.org/apps 
- TELEGRAM_SESSION = "pixel_map" - файл сессии бота

Запуск телеграм бота: 
- `cd src`
- `python pixel_bot/main.py`